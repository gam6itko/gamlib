<?php
namespace Gamlib\Db;

use Gamlib\ArrObj;

abstract class Entity extends ArrObj
{
    /**
     * @var array[key => default]
     */
    protected static $properties = array(
//        'id' => 0,
//        'name' => 'name'
    );

    protected static $tableName = "";

    protected static $primaryKeyField = "id";


    /**
     * @param array $data
     */
    public function __construct(array $data = array())
    {
        $d = array();
        foreach (static::$properties as $k => $v) {
            $d[$k] = array_key_exists($k, $data) ? $data[$k] : $v;
        }
        parent::__construct($d);
    }

    /**
     * @deprecated use getProperties to get array
     * @param string|null $dbName
     * @return string
     */
    public static function getFields($dbName = null)
    {
        $result = static::getProperties($dbName);
        return implode(', ', $result);
    }

    /**
     * @deprecated дурацкий метод
     * @return string
     */
    public function getTokens()
    {
        $result = array();
        foreach ($this->data as $k => $d) {
            $result[] = ":$k";
        }
        return implode(', ', $result);
    }

    /**
     * @deprecated
     * @return array
     */
    public function getValues()
    {
        $result = array();
        foreach ($this->data as $k => $d) {
            $result[] = array(":$k", $d);
        }
        return $result;
    }

    public static function getTableName()
    {
        return static::$tableName;
    }

    public static function getPrimaryKeyField()
    {
        return static::$primaryKeyField;
    }


    /**
     * @param null $dbName - название базы данных, которое будет добавлено к полю БД
     * @return array
     */
    public static function getProperties($dbName = null)
    {
        $result = array();
        foreach (static::$properties as $k => $d) {
            $result[] = (empty($dbName) ? "" : "$dbName.") . "$k";
        }
        return $result;
    }
}