<?php
namespace Gamlib\Db;

use Gamlib\Db;
use ReflectionClass;

class EntityRepository
{
    /**
     * @var Db
     */
    protected $db;

    protected $ref;

    /**
     * @param Db $db
     * @param string $entityClass
     * @throws Exception
     */
    public function __construct(Db $db, $entityClass)
    {
        $this->db = $db;

        $this->ref = new ReflectionClass($entityClass);
        if (!$this->ref->isSubclassOf("Gamlib\\Db\\Entity")) {
            throw new Exception("`$entityClass` is not child of `Gamlib\\Dd\\Entity`");
        }
    }

    /**
     * @param array $criteria - ассоциативный массив поле => значение. Склеивается по средствам `AND`
     * @return array[Entity]
     * @throws Exception
     */
    public function findBy(array $criteria)
    {
        $result = array();
        $db = $this->findPrepare($criteria);

        if ($db->rowCount() == 0) {
            return null;
        }

        while ($tmp = $db->fetch()) {
            $result[] = $this->ref->newInstance($tmp);
        }

        return $result;
    }

    /**
     * @param array $criteria
     * @return Entity
     * @throws Exception
     */
    public function findOneBy(array $criteria)
    {
        $db = $this->findPrepare($criteria);
        $tmp = $db->fetchFirstRow();

        if ($db->rowCount() == 0) {
            return null;
        }

        $result = $this->ref->newInstance($tmp);

        return $result;
    }

    public function persist(Entity $ent)
    {
        $db = $this->update($ent);

        if ($db->rowCount() == 0) {
            $db = $this->insert($ent);
        }

        return $db;
    }

    /**
     * @param Entity $ent
     * @return Db
     * @throws Exception
     */
    public function insert(Entity $ent)
    {
        $tableName = $this->tableName();
        //$fields = implode(', ', $ent->getProperties());

        $fieldsArr = array();
        $tokensArr = array();
        $binds = array();
        foreach ($ent->getProperties() as $i => $fieldName) {
            $fieldsArr[] = "`$fieldName`";
            $token = ":$fieldName";
            $tokensArr[] = $token;
            $binds[] = array($token, $ent->get($fieldName));
        }
        $fields = implode(', ', $fieldsArr);
        $tokens = implode(', ', $tokensArr);

        return $this->db->prepare("INSERT INTO $tableName($fields) VALUES($tokens);")
            ->bindValues($binds)
            ->execute();
    }

    /**
     * @param Entity $ent
     * @return Db
     * @throws Exception
     */
    public function update(Entity $ent)
    {
        $tableName = $this->tableName();
        $pKey = $ent->getPrimaryKeyField();

        $setsArr = array();
        $binds = array();
        foreach ($ent->getProperties() as $i => $fieldName) {
            $token = ":$fieldName";
            $setsArr[] = "`$fieldName` = $token";
            $binds[] = array($token, $ent->get($fieldName));
        }
        $sets = implode(", ", $setsArr);

        return $this->db->prepare("UPDATE $tableName SET $sets WHERE $pKey = :pk;")
            ->bindValues($binds)
            ->bindValue(":pk", $ent[$pKey])
            ->execute();
    }

    /**
     * @param array $criteria
     * @return Db
     * @throws Exception
     */
    protected function findPrepare(array $criteria)
    {
        if (empty($criteria)) {
            throw new Exception("empty criteria");
        }

        $tableName = $this->tableName();
        $fieldsArr = $this->ref->getMethod('getProperties')->invoke(null);
        $fields = implode(', ', $fieldsArr);

        $whereArr = array();
        $binds = array();
        foreach ($criteria as $k => $v) {
            $whereArr[] = "$k = :$k";
            $binds[] = array(":$k", $v);
        }
        $where = implode(" AND ", $whereArr);

        return $this->db
            ->prepare("SELECT $fields FROM $tableName WHERE $where;")
            ->bindValues($binds)
            ->execute();
    }

    protected function tableName()
    {
        $tableName = $this->ref->getMethod('getTableName')->invoke(null);
        if (!$tableName) {
            $tableName = $this->ref->getShortName();
        }
        return $tableName;
    }

}