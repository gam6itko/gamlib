<?php
namespace Gamlib;

use DateTime;

class Helper {

    public static function checkEmpty($data, $check, $failOnExistance = true)
    {
        if (!is_array($check)) {
            $check = array($check);
        }
        $result = array();
        foreach ($check as $checkKey) {
            if (!array_key_exists($checkKey, $data) || $data[$checkKey] === null || $data[$checkKey] === "")
                $result[] = $checkKey;
        }

        if ($failOnExistance && count($result) > 0) {
            throw new GamlibException(sprintf('No required params. [%s]', implode(',', $result)));
        }
        return $result;
    }

    /**
     * Преобразует строку в ассоциативный массив
     * @param string $params - строка типа "par1=val1;par2=val2;"
     * @return array
     */
    public static function parseParams($params)
    {
        if ($params != "") {
            $pairs  = rtrim($params, ";");
            $pairs  = explode(";", $pairs);
            $params = array();
            foreach ($pairs as $pair) {
                if ($pair != "") {
                    $pair             = explode("=", $pair);
                    $params[$pair[0]] = $pair[1];
                }
            }
        }
        return $params;
    }

    /**
     * Возвращает склееную строку со значениям $key двумерного массива.
     * Некий аналог функции MySQL Group_Concat
     * @param $array - Двумерный массив.
     * @param $key - Ключ array[$i][$key]
     * @param string $delim
     * @return string
     */
    public static function concatVals($array, $key, $delim = ',')
    {
        $result = "";
        foreach ($array as $row) {
            $result .= $row[$key] . $delim;
        }
        $result = rtrim($result, $delim);
        return $result;
    }

    public static function getRandNumber($cnt = 20)
    {
        $chars = "1234567890";
        $res   = "";
        for ($i = 0; $i < $cnt; $i++) {
            $res .= $chars[rand(0, strlen($chars) - 1)];
        }

        return $res;
    }

    /**
     * Проверяет путь к каталогу на наличие. Если не существует, то создает его и все недостающие.
     * @param $dirPath путь к каталогу
     * @deprecated use Gamlib\Util\Files
     */
    public static function prepareDir($dirPath)
    {
        $dirPath = trim($dirPath, '/');
        $dirs    = explode('/', $dirPath);
        $dirPath = "";
        for ($i = 0; $i < count($dirs); $i++) {
            $dirPath .= $dirs[$i] . "/";
            if (!file_exists($dirPath)) {
                mkdir($dirPath);
            }
        }
    }

    public static function explodeEnd($delimiter, $string, $limit = null)
    {
        $arr = explode($delimiter, $string); //echo var_export($arr,true)."\n";
        $ind = count($arr) - $limit;
        if (!isset($limit) || $ind <= 0) {
            return $arr;
        } //echo $ind."\n";

        $first  = implode($delimiter, array_splice($arr, 0, $ind + 1)); //echo $first."\n";
        $result = array_slice($arr, 0); //echo var_export($result,true)."\n";
        array_unshift($result, $first);
        return $result;
    }

    /**
     * @return string
     * @deprecated use Gamlib\Util\Files
     */
    public static function pathCombine()
    {
        $result = "";
        foreach (func_get_args() as $row) {
            $result .= self::trimEnd($row, "/\\") . DIRECTORY_SEPARATOR;
        }
        return rtrim($result, "/\\");
    }

    /**
     * The function is_date() validates the date and returns true or false
     * @param $str sting expected valid date format
     * @return bool returns true if the supplied parameter is a valid date otherwise false
     */
    public static function is_date($str)
    {
        try {
            $dt = new DateTime(trim($str));
        } catch (\Exception $e) {
            return false;
        }
        $month = $dt->format('m');
        $day   = $dt->format('d');
        $year  = $dt->format('Y');
        if (checkdate($month, $day, $year)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Необходимо вызвать в начале старта скрипта, чтобы потом узнать время выполнения.
     * Работает в купе с getExecTime
     * @return mixed
     */
    public static function getStartTime()
    {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];
        return $mtime;
    }

    /**
     * Возвращает время выполнения скрипта
     * @param $starttime-Время потученное от функции getStartTime()
     * @return mixed
     */
    public static function getExecTime($starttime)
    {
        $mtime   = microtime();
        $mtime   = explode(" ", $mtime);
        $mtime   = $mtime[1] + $mtime[0];
        $endtime = $mtime;
        return $endtime - $starttime;
    }

    public static function extractUri($uri, &$site, &$port, &$location)
    {
        $port     = 80;
        $tmp      = self::trimStart($uri, "http://");
        $tmp      = explode("/", $tmp, 2);
        $location = "/" . (isset($tmp[1]) ? $tmp[1] : "");
        if (strpos($tmp[0], ":")) {
            $tmp2 = explode(":", $tmp[0]);
            $site = $tmp2[0];
            $port = $tmp2[1];
        } else {
            $site = $tmp[0];
        }
    }



    /**
     * @param $string
     * @return string
     * @deprecated use Gamlib\Util\Strings
     */
    public static function rus2translit($string)
    {
        $converter = array(
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ы' => 'y',
            'ь' => "'",
            'ъ' => "'",
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'E',
            'Ж' => 'Zh',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'Y',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'Ch',
            'Ш' => 'Sh',
            'Щ' => 'Sch',
            'Ы' => 'Y',
            'Ь' => "'",
            'Ъ' => "'",
            'Э' => 'E',
            'Ю' => 'Yu',
            'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    /**
     * @param $source
     * @param $trim
     * @param null $limit
     * @return string
     * @deprecated use Gamlib\Util\Strings
     */
    public static function trimStart($source, $trim, $limit = null)
    {
        while (substr($source, 0, strlen($trim)) == $trim && (!isset($limit) || $limit > 0)) {
            //if(substr($source,0,strlen($source))==$trim)
            //{
            $source = substr($source, strlen($trim), strlen($source));
            if (isset($limit)) {
                $limit--;
            } //}
            else {
                break;
            }
        }
        return $source;
    }

    /**
     * @param $source
     * @param $trim
     * @param null $limit
     * @return string
     * @deprecated use Gamlib\Util\Strings
     */
    public static function trimEnd($source, $trim, $limit = null)
    {
        while (($ind = strrpos($source, $trim)) && (!isset($limit) || $limit > 0)) {
            if (substr($source, $ind, strlen($source)) == $trim) {
                $source = substr($source, 0, $ind);
                if (isset($limit)) {
                    $limit--;
                }
            } else {
                break;
            }
        }
        return $source;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     * @deprecated use Gamlib\Util\Strings
     */
    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     * @deprecated use Gamlib\Util\Strings
     */
    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * Возвращает только цифры в строке
     * @param $str
     * @return mixed
     * @deprecated use Gamlib\Util\Strings
     */
    public static function numbersOnly($str)
    {
        return preg_replace('/[^0-9,.]/','',$str);
    }

    /**
     * @param $str
     * @param bool|true $title
     * @return string
     * @deprecated use Gamlib\Util\Strings
     */
    public static function toCamelCase($str, $title = true)
    {
        $result = '';
        $arr = explode('_', $str);
        for ($i = 0; $i < count($arr); $i++) {
            if ($i == 0 && !$title) {
                $result .= mb_convert_case($arr[$i], MB_CASE_LOWER);
                continue;
            }
            $result .= mb_convert_case($arr[$i], MB_CASE_TITLE);
        }
        return $result;
    }


}