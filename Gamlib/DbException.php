<?php
namespace Gamlib;

use \Exception;

/**
 * Class DbException
 * @package Gamlib
 * @deprecated
 */
class DbException extends Exception
{
    private $_request = '';
    private $_binds = '';

    public function __construct($message, $code = 0, $request = "", $binds = null)
    {
        parent::__construct($message, $code);
        $this->_request = $request;
        $this->_binds = $binds;
    }

    public function getRequest()
    {
        return $this->_request;
    }

    public function __toString()
    {
        $result = parent::__toString() . PHP_EOL
            . "Request: " . $this->_request . PHP_EOL
            . "Binds: " . PHP_EOL;
        foreach ($this->_binds as $k => $v) {
            $result .= "\t$k : $v" . PHP_EOL;
        }
        return $result;
    }
}