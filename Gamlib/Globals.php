<?php
namespace Gamlib;

/**
 * Class Globals - Для хранения глобальных переменных.
 * @package Gamlib
 */
class Globals {

    protected static $_vars = array();

    public static function set($name,$value,$overwrite=false)
    {
        if (isset(self::$_vars[$name]) && !$overwrite){
            trigger_error('Global var `$name` already axists, and can not be overwriting');
            return;
        }
        self::$_vars[$name] = $value;
    }

    public static function get($name)
    {
        if (isset(self::$_vars[$name])){
            return self::$_vars[$name];
        }
        return null;
    }
} 