<?php
namespace Gamlib\Kernel;


use Gamlib\ArrObj;
use Gamlib\GamlibException;
use Gamlib\Structure\AppConfig;
use Gamlib\Util\Strings;
use ReflectionClass;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

class Application
{

    protected $rootDir;

    /**
     * @var array
     */
    protected $bundles = array();

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ContainerBuilder
     */
    protected $container;

    /**
     * @var AppConfig
     */
    protected $config;

    public function __construct($rootPath = null)
    {
        if ($rootPath === null) {
            $reflector = new \ReflectionClass($this);
            $this->rootDir = dirname(dirname($reflector->getFileName()));
        } else if (!file_exists($rootPath)) {
            throw new GamlibException("incorrect root path `$rootPath`");
        } else {
            $this->rootDir = $rootPath;
        }

        $this->container = $this->initContainer();
        $this->request = Request::createFromGlobals();

        $this->readConfig();

        $this->container->set('app', $this);

        $this->addBundles();
    }

    public function run()
    {
        $routing = $this->findRouting($this->request->getUri());

        if (!$routing) {
            throw new GamlibException('incorrect routing: ' . $this->request->getUri());
        }

        $this->checkRoutingAccess($routing);
        $result = $this->executeAction($routing);

        return $result;
    }

    public function getRootDir()
    {
        return $this->rootDir;
    }

    public function getDir_App()
    {
        return $this->rootDir . DIRECTORY_SEPARATOR . 'app';
    }

    public function getDir_Src()
    {
        return $this->rootDir . DIRECTORY_SEPARATOR . 'src';
    }

    /**
     * @param Bundle|string $bundle
     * @param $specName
     * @return $this
     * @throws GamlibException
     */
    public function addBundle($bundle, $specName = null)
    {
        if (is_string($bundle)) {
            if (!class_exists($bundle)) {
                throw new GamlibException("undefined class `$bundle`");
            }
            $refClass = new ReflectionClass($bundle);
            if (!$refClass->isSubclassOf('Gamlib\Kernel\Bundle')) {
                throw new GamlibException("bundle is not subclass");
            }
            $bundle = $refClass->newInstance($this->container);
        } else if (!is_subclass_of($bundle, 'Gamlib\Kernel\Bundle')) {
            throw new GamlibException('bundle is not inherited from Gamlib\\Kerbel\\Bundle');
        }
        if ($specName === null || !is_string($specName)) {
            $specName = $bundle->getName();
        }
        $this->bundles[$specName] = $bundle;
        return $this;
    }

    /**
     * @param $name
     * @return Bundle
     */
    public function getBundle($name)
    {
        return $this->bundles[$name];
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    public function getBundles()
    {
        return $this->bundles;
    }

    public function redirect($url, $statusCode = 303)
    {
        header('Location: ' . $url, true, $statusCode);
        exit();
    }

    public function getConfig()
    {
        return $this->config;
    }


    protected function addBundles()
    {
        foreach ($this->config->bundles as $name => $b) {
            $this->addBundle($b, $name);
        }
    }

    protected function readConfig()
    {
        $configFile = $this->getDir_App() . '/config/config.yml';
        if (!file_exists($configFile)) {
            return;
        }

        $arr = Yaml::parse(file_get_contents($configFile));
        $this->config = new ArrObj($arr);
    }

    protected function initContainer()
    {
        $container = new ContainerBuilder();

        $container->setParameter('kernel.root_dir', $this->rootDir);


        $loader = new YamlFileLoader($container, new FileLocator($this->getDir_App() . '/config'));
        $loader->load('parameters.yml');
        $loader->load('services.yml');

        return $container;
    }

    protected function executeAction(array $routing)
    {
        if (empty($routing['method'])) {
            $routing['method'] = 'index';
        }
        if (!Strings::endsWith($routing['method'], 'Action')) {
            $routing['method'] .= 'Action';
        }

        $ctrl = new $routing['class']($this->container);

        if (!method_exists($ctrl, $routing['method'])) {
            trigger_error("method not exixts `{$routing['class']}` `{$routing['method']}`. route: {$routing['url']['path']}", E_USER_WARNING);
            if ($routing['url']['path'] != '/') {
                $this->redirect('/');
            }
        }

        return call_user_func_array(array($ctrl, $routing['method']), array($this->request, $routing['matches']));
    }

    protected function findRouting($url)
    {
        $url = parse_url($url);
        $routings = Yaml::parse($this->getDir_App() . '/config/routing.yml');

        $res = null;
        $matches = array();
        foreach ($routings as $r) {
            if (array_key_exists('prefix', $r)) {
                $r['regexp'] = "^" . preg_quote($r['prefix'], "/") . "(.*)$";
            }
            if (array_key_exists('regexp', $r)) {
                if ($p = preg_match("/{$r['regexp']}/", $url['path'], $matches)) {
                    $res = $r;
                    $res['matches'] = $matches;
                    break;
                }
            }
        }
        unset($r);

        if ($res) {
            $res['url'] = $url;

            if (!array_key_exists('method', $res) && array_key_exists('prefix', $res)) {
                $urlArgs = explode('/', ltrim(Strings::trimStart($res['url']['path'], $res['prefix']), '/'));
                $res['method'] = array_splice($urlArgs, 0, 1)[0];
                $res['method'] = str_replace('.', '_', $res['method']);
            }
        }

        return $res;
    }

    protected function checkRoutingAccess($routing)
    {
    }
}