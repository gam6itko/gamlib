<?php
namespace Gamlib\Kernel;

use ReflectionClass;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Twig_Environment;

class Bundle extends ContainerAware
{
    /**
     * @var string
     */
    protected $location;

    protected $name;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->extractName();
        //добавляем папку с шаблонами в Twig, если она существует
        if ($tPath = realpath($this->getPath_Resources('views'))) {
            //todo dev-log

            /** @var Twig_Environment $twig */
            $twig = $this->container->get('twig');
            $twig->getLoader()->addPath(
                $tPath,
                $this->getName()
            );
        }

        $this->loadConfigs();
    }

    protected function loadConfigs()
    {
        $classPath = $this->container->get('app')->getDir_Src() . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, get_class($this));
        $path = dirname($classPath) . '/Resources/config';
        if (!$path) {
            return;
        }

        $loader = new YamlFileLoader($this->container, new FileLocator($path));

        if (file_exists("$path/parameters.yml")) {
            $loader->load('parameters.yml');
        }
        if (file_exists("$path/services.yml")) {
            $loader->load('services.yml');
        }
    }


    public function getName()
    {
        return $this->name;
    }

    public function getPath_Resources($add = null)
    {
        $res = 'Resources';
        if ($add) {
            $res .= DIRECTORY_SEPARATOR . trim($add, DIRECTORY_SEPARATOR);
        }
        return $this->location . DIRECTORY_SEPARATOR . $res;
    }

    protected function extractName()
    {
        $reflector = new ReflectionClass($this);

        $this->location = dirname($reflector->getFileName());
        $this->name = mb_strtolower(str_replace('Bundle', '', $reflector->getShortName()));
    }

    protected function log($message, $level)
    {
        if ($this->container->has('logger')) {
            $this->container->get('logger')->log($level, $message);
        }
    }
}