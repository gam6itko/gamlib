<?php
namespace Gamlib\Kernel;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Controller extends ContainerAware
{

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
} 