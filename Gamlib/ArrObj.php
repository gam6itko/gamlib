<?php
namespace Gamlib;

class ArrObj implements \ArrayAccess
{

    /**
     * @var array
     */
    protected $data;

    public function __construct(array $data = array())
    {
        $this->data = $data;
    }

    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function set($pName, $value)
    {
        $names = explode('.', $pName);
        if (!is_array($names)) {
            $names = array($pName);
        }

        $val = &$this->data;
        $len = count($names);
        foreach ($names as $i => $n) {
            // если последний индекс
            if (($i + 1) == $len) {
                $val[$n] = $value;
            } else {
                if (!array_key_exists($n, $val)) {
                    $val[$n] = array();
                }
                $val = &$val[$n];
            }
        }

        return $this;
    }

    public function get($pName, $default = null)
    {
        $names = explode('.', $pName);
        if (!is_array($names)) {
            $names = array($pName);
        }

        $val = $this->data;
        foreach ($names as $n) {
            if (is_array($val) && array_key_exists($n, $val)) {
                $val = $val[$n];
                continue;
            }
            return $default;
        }

        return $val !== null ? $val : $default;
    }

    public function has($pName)
    {
        $result = $this->get($pName);
        return $result != null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    public function all()
    {
        return $this->data;
    }

    public function json()
    {
        return json_encode($this->data);
    }
}