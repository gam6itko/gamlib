<?php
namespace Gamlib;

use Gamlib\Db\EntityRepository;
use Gamlib\Db\Exception;
use PDO;

/**
 * Class Db
 * @version 1.7
 */
class Db
{
    protected static $_instance = array();

    protected $_inTransaction = false; //Показывает, что текущее состояние - в транзакции
    protected $request = '';
    protected $_binds = array();
    /**
     * @var null|PDO
     */
    protected $_db = null;

    /**@var \PDOStatement $stmt */
    private $stmt = '';

    //<editor-fold desc="static">

    /**
     * @param string $connName
     * @throws Exception
     * @return Db|null
     */
    public static function get($connName = "default")
    {
        if (!isset(self::$_instance[$connName])) {
            throw new Exception('Incorrect connection Name');
        }
        if (is_array(self::$_instance[$connName])) {
            list($database, $host, $username, $passwd, $options) = self::$_instance[$connName];
            self::$_instance[$connName] = new Db($database, $host, $username, $passwd, $options);
        }

        if (empty(self::$_instance[$connName])) {
            return null;
        }
        return self::$_instance[$connName];
    }

    public static function has($connName)
    {
        return array_key_exists($connName, self::$_instance);
    }

    public static function init($database, $host = 'localhost', $username = 'root', $passwd = '', $options = array(), $connName = "default")
    {
        self::$_instance[$connName] = array($database, $host, $username, $passwd, $options);
    }

    //</editor-fold desc="static">


    public function __construct($database, $host = 'localhost', $username = 'root', $passwd = '', $options = array(), $engine = 'mysql')
    {
        $this->_db = new PDO("$engine:dbname=$database;host=$host;charset=utf8", $username, $passwd, $options);
    }

    /**
     * @return null|PDO
     */
    public function getPdo()
    {
        return $this->_db;
    }

    /**
     * @return \PDOStatement
     */
    public function getStmt()
    {
        return $this->stmt;
    }

    /**
     * @param $request
     * @return Db
     */
    public function prepare($request)
    {
        $this->request = $request; //Сохраним для вывода в лог при ошибке
        $this->stmt = $this->_db->prepare($request);
        $this->_binds = array();
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @param int $type
     * @throws Exception
     * @return Db
     */
    public function bindValue($name, $value, $type = PDO::PARAM_STR)
    {
        if (!$this->stmt->bindValue($name, $value, $type)) {
            throw new Exception('BindValue error');
        }
        $this->_binds[$name] = $value;
        return $this;
    }

    public function bindValues(array $bindValues)
    {
        foreach ($bindValues as $val) {
            $this->bindValue($val[0], $val[1], (isset($val[2]) ? $val[2] : PDO::PARAM_STR) );
        }
        return $this;
    }

    /**
     * @param null $request
     * @return Db $this
     * @throws Exception
     */
    public function execute($request = null)
    {
        if (!empty($request)) {
            $this->request = $request;
            $this->stmt = $this->_db->prepare($request);
        }
        $execution = $this->stmt->execute();

        if (!$execution) {
            throw new Exception($this->stmt->errorInfo()[2], $this->stmt->errorInfo()[1], $this->request, $this->_binds);
        }

        return $this;
    }

    public function query($request)
    {
        $this->request = $request;
        $this->stmt = $this->_db->query($request);

        if (!$this->stmt) {
            throw new Exception($this->_db->errorInfo()[2], $this->_db->errorInfo()[1], $this->request);
        }
        return $this;
    }

    public function fetch($type = PDO::FETCH_ASSOC)
    {
        return $this->stmt->fetch($type);
    }

    public function fetchAll($type = PDO::FETCH_ASSOC)
    {
        return $this->stmt->fetchAll($type);
    }

    /**
     * Возвращает первую строку
     * @param int $type
     * @return mixed
     */
    public function fetchFirstRow($type = PDO::FETCH_ASSOC)
    {
        $result = $this->stmt->fetch($type);
        $this->stmt->closeCursor();
        return $result;
    }

    /**
     * Возвращает значение первого столбца первой строки
     * @return mixed
     */
    public function fetchScalar()
    {
        $result = $this->fetchFirstRow(PDO::FETCH_NUM);
        return $result[0];
    }

    /**
     * Возвращает количество строк, затронутых в ходе INSERT, UPDATE или DELETE
     *
     * @return mixed
     */
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }


    public function lastInsertId()
    {
        return $this->_db->lastInsertId();
    }

    public function beginTransaction()
    {
        if (!$this->_inTransaction) {
            $this->_db->beginTransaction();
            $this->_inTransaction = true;
        }
    }

    public function rollBack()
    {
        if ($this->_inTransaction) {
            $this->_db->rollBack();
            $this->_inTransaction = false;
        }
    }

    public function commit()
    {
        if ($this->_inTransaction) {
            $this->_db->commit();
            $this->_inTransaction = false;
        }
    }

    /**
     * @param $className
     * @return EntityRepository
     */
    public function getRepository($className)
    {
        return new EntityRepository($this, $className);
    }

}
