<?php
namespace Gamlib\Util;

use Gamlib\Helper;

class Files
{
    /**
     * ��������� ���� � �������� �� �������. ���� �� ����������, �� ������� ��� � ��� �����������.
     * @param $dirPath ���� � ��������
     * @tutorial ""
     */
    public static function prepareDir($dirPath)
    {
        $dirPath = trim($dirPath, DIRECTORY_SEPARATOR);
        $dirs    = explode(DIRECTORY_SEPARATOR, $dirPath);
        $dirPath = "";
        for ($i = 0; $i < count($dirs); $i++) {
            $dirPath .= $dirs[$i] . DIRECTORY_SEPARATOR;
            if (!file_exists($dirPath)) {
                mkdir($dirPath);
            }
        }
    }

    public static function pathCombine()
    {
        $result = "";
        foreach (func_get_args() as $row) {
            $result .= Helper::trimEnd($row, "/\\") . DIRECTORY_SEPARATOR;
        }
        return rtrim($result, "/\\");
    }

    /**
     * ���������� �������� �����
     * @param $source - ���������� ��
     * @param $dest - ���������� �
     */
    public static function copy_r($source, $dest)
    {
        if (is_dir($source)) {
            $dir_handle = opendir($source);
            while ($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (is_dir($source . DIRECTORY_SEPARATOR . $file)) {
                        if (!is_dir($dest . DIRECTORY_SEPARATOR . $file)) {
                            mkdir($dest . DIRECTORY_SEPARATOR . $file);
                        }
                        self::copy_r($source . DIRECTORY_SEPARATOR . $file, $dest . DIRECTORY_SEPARATOR . $file);
                    } else {
                        if (!file_exists($dest)){
                            mkdir($dest, 0777, true);
                        }
                        copy($source . DIRECTORY_SEPARATOR . $file, $dest . DIRECTORY_SEPARATOR . $file);
                    }
                }
            }
            closedir($dir_handle);
        } else {
            mkdir($dest, 0777, true);
            copy($source, $dest);
        }
    }


    public static function delete_directory($dirname)
    {
        $dir_handle = false;
        if (is_dir($dirname)) {
            $dir_handle = opendir($dirname);
        }
        if (!$dir_handle) {
            return false;
        }

        $excludes = array('.', '..');
        while ($file = readdir($dir_handle)) {
            if (!in_array($file, $excludes)) {
                if (!is_dir($dirname . DIRECTORY_SEPARATOR . $file)) {
                    unlink($dirname . DIRECTORY_SEPARATOR . $file);
                } else {
                    self::delete_directory($dirname . DIRECTORY_SEPARATOR . $file);
                }
            }
        }

        closedir($dir_handle);
        rmdir($dirname);

        return true;
    }

    /**
     * ������� ����� ���� ���� ��� symlink
     * @param $dir
     */
    /**
     * ������� ����� ���� ���� ��� symlink
     * @param $dir
     */
    public static function rmdir_r($dir)
    {
        if (is_link($dir)) {
            rmdir($dir);
            return;
        }

        if (is_dir($dir)) {
            foreach (scandir($dir) as $file) {
                if ('.' === $file || '..' === $file) {
                    continue;
                }

                self::rmdir_r("$dir/$file");
            }

            rmdir($dir);
        } else if (is_file($dir)) {
            // deleting file
            unlink($dir);
        }
    }
}