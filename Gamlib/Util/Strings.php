<?php
namespace Gamlib\Util;

class Strings
{


    /**
     * @param $string
     * @return string
     */
    public static function rus2translit($string)
    {
        $converter = array(
            '�' => 'a',
            '�' => 'b',
            '�' => 'v',
            '�' => 'g',
            '�' => 'd',
            '�' => 'e',
            '�' => 'e',
            '�' => 'zh',
            '�' => 'z',
            '�' => 'i',
            '�' => 'y',
            '�' => 'k',
            '�' => 'l',
            '�' => 'm',
            '�' => 'n',
            '�' => 'o',
            '�' => 'p',
            '�' => 'r',
            '�' => 's',
            '�' => 't',
            '�' => 'u',
            '�' => 'f',
            '�' => 'h',
            '�' => 'c',
            '�' => 'ch',
            '�' => 'sh',
            '�' => 'sch',
            '�' => 'y',
            '�' => "'",
            '�' => "'",
            '�' => 'e',
            '�' => 'yu',
            '�' => 'ya',
            '�' => 'A',
            '�' => 'B',
            '�' => 'V',
            '�' => 'G',
            '�' => 'D',
            '�' => 'E',
            '�' => 'E',
            '�' => 'Zh',
            '�' => 'Z',
            '�' => 'I',
            '�' => 'Y',
            '�' => 'K',
            '�' => 'L',
            '�' => 'M',
            '�' => 'N',
            '�' => 'O',
            '�' => 'P',
            '�' => 'R',
            '�' => 'S',
            '�' => 'T',
            '�' => 'U',
            '�' => 'F',
            '�' => 'H',
            '�' => 'C',
            '�' => 'Ch',
            '�' => 'Sh',
            '�' => 'Sch',
            '�' => 'Y',
            '�' => "'",
            '�' => "'",
            '�' => 'E',
            '�' => 'Yu',
            '�' => 'Ya',
        );
        return strtr($string, $converter);
    }

    /**
     * @param $source
     * @param $trim
     * @param null $limit
     * @return string
     */
    public static function trimStart($source, $trim, $limit = null)
    {
        while (substr($source, 0, strlen($trim)) == $trim && (!isset($limit) || $limit > 0)) {
            //if(substr($source,0,strlen($source))==$trim)
            //{
            $source = substr($source, strlen($trim), strlen($source));
            if (isset($limit)) {
                $limit--;
            } //}
            else {
                break;
            }
        }
        return $source;
    }

    /**
     * @param $source
     * @param $trim
     * @param null $limit
     * @return string
     */
    public static function trimEnd($source, $trim, $limit = null)
    {
        while (($ind = strrpos($source, $trim)) && (!isset($limit) || $limit > 0)) {
            if (substr($source, $ind, strlen($source)) == $trim) {
                $source = substr($source, 0, $ind);
                if (isset($limit)) {
                    $limit--;
                }
            } else {
                break;
            }
        }
        return $source;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * ���������� ������ ����� � ������
     * @param $str
     * @return mixed
     */
    public static function numbersOnly($str)
    {
        return preg_replace('/[^0-9,.]/','',$str);
    }

    /**
     * @param $str
     * @param bool|true $title
     * @return string
     */
    public static function toCamelCase($str, $title = true)
    {
        $result = '';
        $arr = explode('_', $str);
        for ($i = 0; $i < count($arr); $i++) {
            if ($i == 0 && !$title) {
                $result .= mb_convert_case($arr[$i], MB_CASE_LOWER);
                continue;
            }
            $result .= mb_convert_case($arr[$i], MB_CASE_TITLE);
        }
        return $result;
    }

    public static function toUnderscoreCase($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}