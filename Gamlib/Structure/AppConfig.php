<?php
namespace Gamlib\Structure;

use Gamlib\ArrObj;

/**
 * Class AppConfig
 * @property string|bool $debug_mode - тип отладки. `false` если prod
 * @property array $bundles - массив классов Bundle, которые надо добавить в Application
 * @property string $auth_controller - класс контроллера отвечающего за авторизацию
 * @property array $user_provider - class
 * @property array $security - массив где ключем является regexp, а значением = [user|anonimous]
 */
class AppConfig extends ArrObj
{
}